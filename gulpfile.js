let projectFolder = require("path").basename(__dirname + " create by Vado");
let srcFolder = "src";

let path = {
  build: {
    html: projectFolder + "/",
    css: projectFolder + "/css/",
    js: projectFolder + "/js/",
    img: projectFolder + "/img/",
    video: projectFolder + "/video/",
    fonts: projectFolder + "/fonts/",
  },
  src: {
    html: [srcFolder + "/*.html", "!" + srcFolder + "/_*.html"],
    css: srcFolder + "/pcss/main.pcss",
    js: srcFolder + "/js/main.js",
    img: srcFolder + "/img/**/*.{jpg,png,svg,gif,ico,webp}",
    video: srcFolder + "/video/**/*",
    fonts: srcFolder + "/fonts/*.ttf",
  },
  watch: {
    html: srcFolder + "/**/*.html",
    css: srcFolder + "/pcss/**/*.pcss",
    js: srcFolder + "/js/**/*.js",
    img: srcFolder + "/img/**/*.{jpg,png,svg,gif,ico,webp}",
    video: srcFolder + "/video/**/*",
  },
  clean: "./" + projectFolder + "/",
};

let { src, dest } = require("gulp"),
  gulp = require("gulp"),
  browsersync = require("browser-sync").create(),
  fileinclude = require("gulp-file-include"),
  del = require("del"),
  postcss = require("gulp-postcss"),
  cssnext = require("cssnext"),
  postcss_import = require("postcss-import"),
  postcss_url = require("postcss-url"),
  postcss_mixins = require("postcss-mixins"),
  postcss_advanced_variables = require("postcss-advanced-variables"),
  postcss_extend_rule = require("postcss-extend-rule"),
  postcss_atroot = require("postcss-atroot"),
  postcss_preset_env = require("postcss-preset-env"),
  postcss_property_lookup = require("postcss-property-lookup"),
  postcss_nested = require("postcss-nested"),
  precss = require("precss"),
  autoprefixer = require("gulp-autoprefixer"),
  group_media = require("gulp-group-css-media-queries"),
  clean_css = require("gulp-clean-css"),
  rename = require("gulp-rename"),
  uglify = require("gulp-uglify-es").default,
  imagemin = require("gulp-imagemin"),
  webp = require("gulp-webp"),
  webphtml = require("gulp-webp-html"),
  ttf2woff = require("gulp-ttf2woff"),
  ttf2woff2 = require("gulp-ttf2woff2"),
  fonter = require("gulp-fonter");

function css() {
  const processors = [
    postcss_import,
    postcss_url,
    postcss_mixins,
    postcss_advanced_variables,
    postcss_extend_rule,
    postcss_atroot,
    postcss_preset_env({
      browsers: [">0.5%"],
      cascade: false,
    }),
    postcss_property_lookup,
    postcss_nested,
    cssnext,
    precss,
  ];
  return src(`${path.src.css}`)
    .pipe(postcss(processors))
    .pipe(
      rename({
        extname: ".css",
      })
    )
    .pipe(group_media())
    .pipe(
      autoprefixer({
        overrideBrowserslist: ["last 5 versions"],
        cascade: true,
      })
    )
    .pipe(dest(path.build.css))
    .pipe(clean_css())
    .pipe(
      rename({
        extname: ".min.css",
      })
    )
    .pipe(dest(path.build.css)) //*Зжатий файл
    .pipe(browsersync.stream());
}

function js() {
  return src(path.src.js)
    .pipe(fileinclude())
    .pipe(dest(path.build.js))
    .pipe(uglify())
    .pipe(
      rename({
        extname: ".min.js",
      })
    )
    .pipe(dest(path.build.js)) //*Зжатий файл
    .pipe(browsersync.stream());
}

function html() {
  return src(path.src.html)
    .pipe(fileinclude())
    .pipe(webphtml())
    .pipe(dest(path.build.html))
    .pipe(browsersync.stream());
}

function images() {
  return src(path.src.img)
    .pipe(
      webp({
        quality: 70,
      })
    )
    .pipe(dest(path.build.img))
    .pipe(src(path.src.img))
    .pipe(
      imagemin({
        progressive: true,
        svgoPlugins: [{ removeViewBox: false }],
        interlaced: true,
        optimizatioLevel: 3, //* 0 to 7
      })
    )
    .pipe(dest(path.build.img))
    .pipe(browsersync.stream());
}

function video() {
  return src(path.src.video)
    .pipe(dest(path.build.video))
    .pipe(browsersync.stream());
}

function fonts(params) {
  src(path.src.fonts).pipe(ttf2woff()).pipe(dest(path.build.fonts));
  return src(path.src.fonts).pipe(ttf2woff2()).pipe(dest(path.build.fonts));
}
//*форматувати otf to ttf
gulp.task("otf2ttf", function () {
  return gulp
    .src([srcFolder + "/fonts/*.otf"])
    .pipe(
      fonter({
        formats: ["ttf"],
      })
    )
    .pipe(dest(srcFolder + "/fonts/"));
});

function watchFiles(params) {
  gulp.watch([path.watch.css], css);
  gulp.watch([path.watch.js], js);
  gulp.watch([path.watch.html], html);
  gulp.watch([path.watch.img], images);
  gulp.watch([path.watch.video], video);
}

function clean(parms) {
  return del(path.clean);
}

function browserSync(params) {
  browsersync.init({
    server: {
      baseDir: "./" + projectFolder + "/",
    },
    port: 3000,
    notify: false,
  });
}

let build = gulp.series(
  clean,
  gulp.parallel(js, css, html, images, video, fonts),
  browserSync
);
let watch = gulp.parallel(build, watchFiles);

exports.js = js;
exports.css = css;
exports.html = html;
exports.images = images;
exports.video = video;
exports.fonts = fonts;
exports.build = build;
exports.watch = watch;
exports.default = watch;
